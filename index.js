const GitTools = require('./lib/gitTools.js');
const inquirer = require('./lib/inquirerTools.js');

// 注意此处目录需要 \\ 否则将会报错
var git = new GitTools(`D:\\WebProject\\byf_web_core_multi_page`);

const options = {
	'checkout (切分支)': 'checkout',
	'pull (拉取代码)': 'pull',
	'add (添加暂存区)': 'add',
	'commit (提交)': 'commit',
	'push (推送)': 'push',
	'status (修改状态)': 'status',
	'branch (查看分支)': 'branch',
	'switchBreach (切换分支并拉取最新代码)': 'switchBreach',
	'自动推送': 'autoUpload',
}

const filterInput = ['add','status','branch'];


async function main() {
	console.clear();

	try {
		console.log('--- git Tools 示例操作 --- ');
		console.log()

		let type = await inquirer.list('请选择你要进行的操作', Object.keys(options));
		type = options[type];

		let result = ''
		if (!filterInput.some(item=>item == type)) {
			result = await inquirer.input(type == 'commit' ? '请输入备注信息' : '请输入分支名称')
		}
		
		let data =  await git[type](result);
		
		console.log('--- 操作完毕 --- ',data);
		console.log()
		
		let isReset = await inquirer.confirm('是否需要重新选择？');
		
		if(isReset){
			main();
		}
		
		
	} catch (err) {
		console.error('流程错误:', err);
	}

}

main();
